import { Collapse } from 'bootstrap/js/dist/collapse';

import { library, dom } from '@fortawesome/fontawesome-svg-core';
import { faClock, faRandom, faKeyboard, faFileAlt, faArrowUp, faPuzzlePiece } from '@fortawesome/free-solid-svg-icons';
import { faGithub, faGitlab, faYoutube, faVimeo } from '@fortawesome/free-brands-svg-icons';

library.add([faClock, faRandom, faKeyboard, faFileAlt, faArrowUp, faPuzzlePiece, faGithub, faGitlab, faYoutube, faVimeo]);
dom.watch();

import './test.scss';